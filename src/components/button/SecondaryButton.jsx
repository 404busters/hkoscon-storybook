import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button';

export default function SecondaryButton({ children }) {
  return (
    <Button className="secondary primary-text">
      { children }
    </Button>
  );
}

SecondaryButton.propTypes = {
  children: PropTypes.node.isRequired,
};
